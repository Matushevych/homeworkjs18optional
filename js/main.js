$(document).ready(function() {

    let scrollLink = $('.scroll');
    let arrow = $('#arrow');

    // Smooth scrolling
    scrollLink.click(function(e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: $(this.hash).offset().top
        }, 1000 );
    });


    // Active link switching
    $(window).scroll(function() {
        let scrollbarLocation = $(this).scrollTop();
        let viewPortHeight = $(window).height();

        if (viewPortHeight <= scrollbarLocation){
            if ($(arrow).css("opacity") === "0"){
            $(arrow).animate({opacity: 1}, 1000);
        }}
        else{
            if ($(arrow).css("opacity") === "1"){
            $(arrow).animate({opacity: 0}, 300);
        }}

        $('.navigation a').each(function() {
            let sectionOffset = $(this.hash).offset().top - 20;
            if (sectionOffset <= scrollbarLocation) {
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active');
            }
        })

    });

    // Toggle Forth-Section
    $(".toggle").click(function(){
        $('#forth-section').slideToggle();
    });
});